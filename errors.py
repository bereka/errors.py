# სავარჯიშო 1
# while True:
#     try:
#         num1 = int(input("Enter the first number: "))
#         num2 = int(input("Enter the second number: "))
#         result = num1 / num2
#
#     except ZeroDivisionError:
#         print("Error: Division by zero. Please try again.")
#
#     except ValueError:
#         print("Error: Value is not a number. Please try again.")
#         continue
#
#     else:
#         print(result)
#         break


# სავარჯიშო 2
# def division(x, y):
#     try:
#         if y == 0:
#             raise ZeroDivisionError("Division by zero")
#
#         if x % y != 0:
#             raise Exception("Division error")
#
#     except Exception as m:
#         return m
#
#     except ZeroDivisionError as m:
#         return m
#
#     except:
#         return "error"
#
#     else:
#         return x / y
#
#
# print(division(20, 3))

# სავარჯიშო 3
# try:
#     index = int(input("Enter the index: "))
#     my_list = [2, 3, 4, 5, 6]
#
#     print(my_list[index])
#
# except IndexError:
#     print("No element with this index")

# სავარჯიშო 4
# try:
#     file = open("myResult.txt", "r")
# except FileNotFoundError:
#     print("Error: There is no file with this name")

# სავარჯიშო 5
# import math
#
# try:
#     a = float(input("Enter the first coefficient: "))
#     b = float(input("Enter the first coefficient: "))
#     c = float(input("Enter the first coefficient: "))
#     discriminant = (b ** 2) - (4 * a * c)
#     if discriminant < 0:
#         raise Exception("Error: Discriminant is negative")
# except ValueError as m:
#     print(m)
#
# except Exception as m:
#     print(m)
#
#
# else:
#     if discriminant == 0:
#         print(-b / (2 * a))
#
#     else:
#         result = [(-b - math.sqrt(discriminant) / (2 * a)), (-b + math.sqrt(discriminant) / (2 * a))]
#         print(result[0])
#         print(result[1])

# სავარჯიშო 6
# try:
#     size1 = float(input("Enter the first number: "))
#     size2 = float(input("Enter the second number: "))
#     size3 = float(input("Enter the third number: "))
#     if size1 + size2 < size3 or size2 + size3 < size1 or + size1 + size3 < size2:
#         raise Exception("Error: The rule for the lengths of the sides of a triangle")
# except Exception as m:
#     print(m)
#
# else:
#     print((size1 + size2 + size3) / 3)


